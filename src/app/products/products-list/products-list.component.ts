import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ProductsServiceService } from "src/app/Services/products-service.service";

@Component({
  selector: "app-products-list",
  templateUrl: "./products-list.component.html",
  styleUrls: ["./products-list.component.scss"],
})
export class ProductsListComponent implements OnInit {
  // products = [
  //   {
  //     title: "Product1",
  //     price: "2000",
  //     category: "tablet",
  //     image:
  //       "https://fakestoreapi.com/img/71-3HjGNDUL._AC_SY879._SX._UX._SY._UY_.jpg",
  //   },
  //   {
  //     title: "Product1",
  //     price: "2000",
  //     category: "tablet",
  //     image:
  //       "https://fakestoreapi.com/img/71-3HjGNDUL._AC_SY879._SX._UX._SY._UY_.jpg",
  //   },
  //   {
  //     title: "Product1",
  //     price: "2000",
  //     category: "tablet",
  //     image:
  //       "https://fakestoreapi.com/img/71-3HjGNDUL._AC_SY879._SX._UX._SY._UY_.jpg",
  //   },
  //   {
  //     title: "Product1",
  //     price: "2000",
  //     category: "tablet",
  //     image:
  //       "https://fakestoreapi.com/img/71-3HjGNDUL._AC_SY879._SX._UX._SY._UY_.jpg",
  //   },
  //   {
  //     title: "Product1",
  //     price: "2000",
  //     category: "tablet",
  //     image:
  //       "https://fakestoreapi.com/img/71-3HjGNDUL._AC_SY879._SX._UX._SY._UY_.jpg",
  //   },
  // ];

  products: any[] = [];
  constructor(
    private productServ: ProductsServiceService,
    private route: Router
  ) {}

  ngOnInit() {
    this.productServ.getProducts().subscribe((res: any) => {
      this.products = res;
    });
  }

  toDetails(product) {
    this.route.navigate([`product-detail/${product.id}`]);
  }
}
