import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ProductsServiceService } from "src/app/Services/products-service.service";

@Component({
  selector: "app-product-details",
  templateUrl: "./product-details.component.html",
  styleUrls: ["./product-details.component.scss"],
})
export class ProductDetailsComponent implements OnInit {
  product: any;
  constructor(
    private ActivatedRoute: ActivatedRoute,
    private productServ: ProductsServiceService
  ) {}

  ngOnInit() {
    this.ActivatedRoute.params.subscribe((res: any) => {
      console.log('res of route: ',res)
      if (res.id) {
        this.getProductdetails(res.id);
      }
    });
  }

  getProductdetails(id) {
    this.productServ.getDetail(id).subscribe(
      (res: any) => {
        console.log("res of detail: ", res);
        this.product = res;
      },
      (err) => {
        console.log("error of getDetail: ", err);
      }
    );
  }

  // used to display rating star item
  newArr(num) {
    return Array(Math.ceil(num))
  }
}
