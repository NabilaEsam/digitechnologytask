import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Alert } from "selenium-webdriver";
import { ProductsServiceService } from "src/app/Services/products-service.service";

@Component({
  selector: "app-add-product",
  templateUrl: "./add-product.component.html",
  styleUrls: ["./add-product.component.scss"],
})
export class AddProductComponent implements OnInit {
    imgURL: any;
  constructor(
    private productServ: ProductsServiceService,
    private route: Router
  ) {}

  //start .. add product
  productForm = new FormGroup({
    title: new FormControl("", [
      Validators.nullValidator,
      Validators.minLength(1),
    ]),
    description: new FormControl("", [
      Validators.required,
      Validators.minLength(1),
    ]),
    category: new FormControl("", [
      Validators.required,
      Validators.minLength(1),
    ]),
    image: new FormControl(null, [Validators.required]),
    price: new FormControl(0, [Validators.required]),
  });
  get title() {
    return this.productForm.get("title");
  }
  get description() {
    return this.productForm.get("description");
  }
  get category() {
    return this.productForm.get("category");
  }

  get image() {
    return this.productForm.get("image");
  }
  get price() {
    return this.productForm.get("price");
  }

  // end .. add product

  ngOnInit() {}
  handleUpload(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      console.log("reader.result: ", reader.result);
      this.imgURL = reader.result;
    };
  }

  addProduct() {
    console.log("product form: ", this.productForm.value);
    this.productServ.addProduct(this.productForm.value).subscribe(
      (res: any) => {
     this.route.navigate(['/'])
      },
      (err) => {
        console.log("error of add product:", err);
      }
    );
  }
}
