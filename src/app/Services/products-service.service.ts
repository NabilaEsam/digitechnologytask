import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class ProductsServiceService {
  constructor(public http: HttpClient) {}

  getProducts() {
    return this.http.get(`https://fakestoreapi.com/products?limit=10`);
  }
  getDetail(id) {
    return this.http.get(`https://fakestoreapi.com/products/${id}`);
  }

  addProduct(values) {
    return this.http.post(`https://fakestoreapi.com/products`, values);
  }
}
