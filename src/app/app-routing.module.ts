import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  // { 
  //   path: '', 
  //   loadChildren: () => import(`./products/add-product/add-product.module`).then(
  //     module => module.AddProductModule
  //   )
  // },

  { 
    path: '', 
    loadChildren: () => import(`./products/products.module`).then(
      module => module.ProductsModule
    )
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
